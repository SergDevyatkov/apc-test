import os

import requests
from pydantic import BaseModel


class ModelOutput(BaseModel):
    t: list[float]
    a: list[float]
    b_flowrate: list[float]


class ModelInput(BaseModel):
    tspan: tuple[float, float]
    flowrate: dict[float, float]


PORT = os.environ["TEST_PORT"]
HOST = os.environ["TEST_HOST"]
URL = f"http://{HOST}:{PORT}/model"


def get_model_output(tspan: tuple[float, float], flowrate: dict[float, float]):
    req = ModelInput(tspan=tspan, flowrate=flowrate)
    r = requests.post(URL, data=req.model_dump_json())
    r.raise_for_status()

    res = ModelOutput(**r.json())
    return res


if __name__ == "__main__":
    res = get_model_output((0.0, 200.0), {0.0: 1.0, 3.0: 2.0, 5.0: 1.1})
